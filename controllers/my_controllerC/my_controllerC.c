/*

  BY TEAM SPECTRAL
  ROUND 2

*/

#include <webots/keyboard.h>
#include <webots/robot.h>
#include <webots/nodes.h>
#include <webots/supervisor.h>
#include <webots/camera.h>
#include <webots/range_finder.h>

#include <arm.h>
#include <base.h>
#include <gripper.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TIME_STEP 16

enum BLOB_TYPE { RED, GREEN, BLUE, NONE };
typedef enum { SEARCHING, MOVING } states;

int pause_counter = 0;
  
// initialization: range finder
int rf_width, rf_height;
states state = SEARCHING;
float depth_resultant = 0;
float distance;
WbDeviceTag range_finder;

// initialization: camera parameters
int width, height;
int red, blue, green;  
WbDeviceTag camera;

enum BLOB_TYPE current_blob;

// initial translation of the obstacle boxes
int i, j;
int no_boxes = 50;
WbNodeRef KKboxes[50];
char Obs_names[50][6];
char tmp[3];
double values[50][3];  
  
void obstacles()
{
  // initialization: obstacle-boxes
  /* random position vectors-values- for obstacles defined as KKBx */
  srand(time(0)); 
  for(i = 0; i<no_boxes; i++){
    char kb[6]={"KKB"};
    sprintf(tmp, "%d", i); 
    strcpy(Obs_names[i], strcat(kb, tmp));   
    for(j = 0; j<3; j++){ 
      values[i][j]= round((rand()%100-49.5))/10.0;
      values[i][1]= 0.041;
    }
  }
  /* setting KKBx translations to above random values */
  for (i = 0; i < no_boxes ; i++) {
    KKboxes[i] = wb_supervisor_node_get_from_def(Obs_names[i]);
    WbFieldRef tr = wb_supervisor_node_get_field(KKboxes[i], "translation");
    wb_supervisor_field_set_sf_vec3f(tr, values[i]);
  }
}

/*
  Initializes the range finder device
*/
void initalizeRangeFinder()
{
  /* get the device, enable it and store data*/
  range_finder = wb_robot_get_device("range-finder");
  wb_range_finder_enable(range_finder, TIME_STEP);
  rf_width = wb_range_finder_get_width(range_finder);
  rf_height = wb_range_finder_get_height(range_finder);
}

/*
  Initializes the camera
*/
void initializeCamera()
{
  /* Get the camera device, enable it, and store its width and height */
  camera = wb_robot_get_device("camera");
  wb_camera_enable(camera, TIME_STEP);
  width = wb_camera_get_width(camera);
  height = wb_camera_get_height(camera);
}

bool seen_green_thing = false;

int touch_counter =0;

int timer = 0;
bool right = false;
bool boxes_touched[50];
int base_touches = 0;
bool left_selected = false;
bool low_green_found = false;

double original_green_sensitivity = 0.8;
double original_low_green_sensitivity = 0.9;
double green_sensitivity = 0.8;
double low_green_sensitivity = 0.9;

int time_without_touch = 0;

/*
  Translates the boxes that have been contacted with the robot
*/
void translateContactedBoxes()
{
  /* code snippet to translate boxes into a different 
  place when contacted with any object after 2s from
  the start of the simulation */
  for (i = 0; i < no_boxes && wb_robot_get_time() > 2.0; i++) {
    int ct = wb_supervisor_node_get_number_of_contact_points(KKboxes[i]);
    
    if (ct > 0) {      
    
      WbFieldRef tr = wb_supervisor_node_get_field(KKboxes[i], "translation");
      //teleport boxes to underworld ;-)  ----disappear

      values[i][1]= -20*values[i][1];
      wb_supervisor_field_set_sf_vec3f(tr, values[i]);
      
      if (boxes_touched[i] == false)
      {        
        touch_counter ++;
                
        boxes_touched[i] = true;
        
        printf("Touched boxes: %i\n", touch_counter);
        
        timer = 0;
        
        left_selected = !left_selected;
        
        if (!seen_green_thing)
        {
           base_touches++; 
           printf("Missed boxes: %i \n", base_touches); 
        }
      }
      
      seen_green_thing = false;      
    }
  }
}

/*
  Analyze the data from the range finder and calculate an average depth
*/
void analyzeDepth(const float *rfimage)
{
  // ananlyse the range finder image to get resultant depth
  rf_width = wb_range_finder_get_width(range_finder);
  rf_height = wb_range_finder_get_height(range_finder);
  depth_resultant = 0;
  
  for (i = rf_width / 3; i < 2 * rf_width / 3; i++)
  {
    for (j = rf_height / 2; j < 3 * rf_height / 4; j++)
    {
      depth_resultant += wb_range_finder_image_get_depth(rfimage, rf_width, i, j);
    } 
  } 
}

int _leftSide = 0;
int _middle = 0;
int _rightSide = 0;

/*
  Calculate the color from the image received from the camera
*/
void analyzeColor(const unsigned char *image)
{
  red = 0;
  green = 0;
  blue = 0;
  
  for (i = width / 3; i < 2 * width / 3; i++) {
    for (j = height / 2; j < 3 * height / 4; j++) {
      red += wb_camera_image_get_red(image, width, i, j);
      blue += wb_camera_image_get_blue(image, width, i, j);
      green += wb_camera_image_get_green(image, width, i, j);
    }
  }
  
  // Calculate which side contains the To turn the arm to the right direction
  for (i = 0; i < width; i++) {
    for (j = 0; j < height; j++) {
      if (i < width / 3)
        _leftSide += wb_camera_image_get_green(image, width, i, j);
      else if (i > width / 3 && i < 2 * width / 3)
        _middle += wb_camera_image_get_green(image, width, i, j);
      else if (i > width * 2 / 3)
        _rightSide += wb_camera_image_get_green(image, width, i, j);
    }
  } 
}

/*
  Get which side contains the most green color
*/
int getSide()
{
  if (_leftSide > _middle && _leftSide > _rightSide)
    return 0;
  else if (_middle > _leftSide && _middle > _rightSide)
    return 1;
  else if (_rightSide > _middle && _rightSide > _leftSide)
    return 2;
  else 
    return 1;
}

/*
  Recognize the more than average color of the image
*/
void blobRecognition()
{
  low_green_found = false;

  if ((red > 3 * green) && (red > 3 * blue))
    current_blob = RED;
  else if ((green > (1/green_sensitivity) * red) && (green > (1/green_sensitivity) * blue))
    current_blob = GREEN;
  else if ((green > (1/low_green_sensitivity) * red) && (green > (1/low_green_sensitivity) * blue))
    low_green_found = true; 
  else if ((blue > 3 * red) && (blue > 3 * green))
    current_blob = BLUE;
  else
    current_blob = NONE;
    
  if (current_blob == GREEN)
  {
    time_without_touch = 0;
  }
}

int num = 0;

/*
  Locomotion of the robot
*/
void locomotion()
{
  if (current_blob != GREEN) 
  {
    if (!seen_green_thing)
    {
      if (!low_green_found)
      {
        // Randomly select the amount of time the base turns and goes forward.
        num = (rand() % 301) + 300;
        
        if (timer % 1000 < num)
        { 
          base_turn_right();
        } 
        else 
        {
          // for detection of walls
          if (depth_resultant > 3000)
          {
            base_forwards();
          }
          else 
          {
            base_turn_right();
          }
        }
      }
      else 
      {
        // If possible green blob is detected (from higher sensitivity data)
        base_forwards();
      }
      
      arm_set_height(ARM_FRONT_PLATE);
    }
    else
    {
       // If already seen a green blob on the camera
      base_forwards();
    }
  }
  else if (current_blob == GREEN) 
  {
    // Green blob found on screen
    base_forwards();
    // Set green thing to true
    seen_green_thing = true;
    arm_set_height(ARM_FRONT_FLOOR); 
    
    // Movement to cover all the area in front of the base.
    if (getSide() == 0)
      arm_set_orientation(ARM_FRONT_LEFT);
    else if (getSide() == 1)
      arm_set_orientation(ARM_FRONT);
    else if (getSide() == 2)
      arm_set_orientation(ARM_FRONT_RIGHT);
      
  }
}

/*
  Increases sensitivity when the robot doesn't find anything for a specific amount of time
*/
void increaseSensitivity()
{
  // When time is less than (TIME_STEP * 625 / 1000) seconds
  if (time_without_touch < 625)
  {
    green_sensitivity = original_green_sensitivity;
    low_green_sensitivity = original_low_green_sensitivity;
  } 
  // After (TIME_STEP * 625 / 1000) seconds
  else if (time_without_touch < 2000)
  {
    printf("Scanning Sensitivity increased 2x\n");
    low_green_sensitivity = 1.2 * original_low_green_sensitivity;
  }
}

/*
  Main loop of the program
*/
void loop()
{
  timer++;
  time_without_touch++;
  
  if (pause_counter >0)
    pause_counter --;
    
  // get the range finder image
  const float *rfimage = wb_range_finder_get_range_image(range_finder);
  // get the camera image
  const unsigned char *image = wb_camera_get_image(camera);
  
  increaseSensitivity();
  translateContactedBoxes();
  analyzeDepth(rfimage);
  analyzeColor(image);
  blobRecognition();
  locomotion();  
}

int main(int argc, char **argv) {
  // initialization: robot
  wb_robot_init();
  
  // Initialize all the devices and sensors
  initalizeRangeFinder(); 
  initializeCamera();  
  obstacles();
  
  base_init();
  arm_init();
  gripper_init();
  
  // Main loop
  while (wb_robot_step(TIME_STEP) != -1) {
    loop();
  }
  
  wb_robot_cleanup();
  return 0;
}
